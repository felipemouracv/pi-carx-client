import { Pi2017ClientPage } from './app.po';

describe('pi2017-client App', () => {
  let page: Pi2017ClientPage;

  beforeEach(() => {
    page = new Pi2017ClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
