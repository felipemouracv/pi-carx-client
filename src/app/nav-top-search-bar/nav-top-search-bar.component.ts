import { Component, OnInit } from '@angular/core';
import {CarrosComponent} from "../carros/carros.component";

@Component({
  selector: 'app-nav-top-search-bar',
  templateUrl: './nav-top-search-bar.component.html',
  styleUrls: ['./nav-top-search-bar.component.css'],
  providers: [CarrosComponent]
})
export class NavTopSearchBarComponent implements OnInit {

  constructor(private carrosComponent:CarrosComponent) { }

  ngOnInit() {
  }

  fullTextSearch(text) {
    this.carrosComponent.fullTextSeach(text);
  }

}
