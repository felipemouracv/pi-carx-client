import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTopSearchBarComponent } from './nav-top-search-bar.component';

describe('NavTopSearchBarComponent', () => {
  let component: NavTopSearchBarComponent;
  let fixture: ComponentFixture<NavTopSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavTopSearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavTopSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
