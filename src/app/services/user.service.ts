import { Injectable }    from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {environment} from "../../environments/environment.prod";
import 'rxjs/add/operator/toPromise';
import {Router} from "@angular/router";

@Injectable()
export class UserService {

  private env;

  constructor(
    private http: Http,
    private router: Router
  ) {
    this.env = environment;
  }

  doRegister(userObject:any): Promise<any[]> {
    return this.http.post(this.env.url_endpoint_api+'user/',JSON.stringify(userObject))
      .toPromise()
      .then(response => response.json().data as any[])
      .catch(this.handleError);
  }

  doLogin(userObject): Promise<any[]> {
    return this.http.post(this.env.url_endpoint_api+'user/getToken',JSON.stringify(userObject))
      .toPromise()
      .then(response => response.json() as any)
      .catch(this.handleError);
  }

  doUpdate(userObject:any): Promise<any[]> {
    return this.http.put(this.env.url_endpoint_api+'user/admin/',JSON.stringify(userObject), this.getAuthorization())
      .toPromise()
      .then(response => response.json() as any)
      .catch(this.handleError);
  }

  getInfoUser(user_id) {
    return this.http.get(this.env.url_endpoint_api+'user/admin/'+user_id,this.getAuthorization())
      .toPromise()
      .then(response => response.json() as any)
      .catch(this.handleError);
  }

  signOut(signOut): Promise<any[]> {
    if(typeof signOut === "object") {
      return this.http.get(this.env.url_endpoint_api+'user/admin/logout',this.getAuthorization())
        .toPromise()
        .then(response => response.json() as any)
        .catch(this.handleError);
    }
    return null;
  }

  getAuthorization() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('token_carx'));
    return new RequestOptions({ headers: headers });
  }

  private handleError(error: any): Promise<any> {
    //redireciona usuario não autorizado
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
