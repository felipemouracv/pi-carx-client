import {Injectable}    from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {environment} from "../../environments/environment.prod";

import 'rxjs/add/operator/toPromise';


@Injectable()
export class CarService {
  private env: any = [];

  constructor(private http: Http) {
    this.env = environment;
  }

  doRemoveImage (id): Promise<any[]> {
    return this.http.delete(this.env.url_endpoint_api + 'image-ads/admin/'+id, this.getAuthorization())
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  doEditAds(object): Promise<any[]> {
    delete object.user;
    delete object.imageAds;
    return this.http.put(this.env.url_endpoint_api + 'ads/admin', JSON.stringify(object), this.getAuthorization())
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  doSaveAds(object): Promise<any[]> {
    return this.http.post(this.env.url_endpoint_api + '/ads/admin', JSON.stringify(object), this.getAuthorization())
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  doRemoveAds(id): Promise<any[]> {
    return this.http.delete(this.env.url_endpoint_api + '/ads/admin/'+id, this.getAuthorization())
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  getBranchs(): Promise<any[]> {
    return this.http.get('https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas')
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  getModel(cod_model): Promise<any[]> {
    return this.http.get('https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas/' + cod_model + '/modelos')
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  getAdsAll(order: string, limit: string, cons: string) {
    return this.http.get(this.env.url_endpoint_api + 'ads/filter/' + order + '/' + limit + '/' + cons)
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  getAds(id) {
    return this.http.get(this.env.url_endpoint_api + 'ads/' + id)
      .toPromise()
      .then(response => response.json() as any[])
      .catch(this.handleError);
  }

  getAuthorization() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('token_carx'));
    return new RequestOptions({headers: headers});
  }

  //
}
