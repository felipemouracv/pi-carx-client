import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTopDefaultComponent } from './nav-top-default.component';

describe('NavTopDefaultComponent', () => {
  let component: NavTopDefaultComponent;
  let fixture: ComponentFixture<NavTopDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavTopDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavTopDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
