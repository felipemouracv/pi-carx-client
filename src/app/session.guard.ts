import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class SessionGuard implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('token_carx') == "" || localStorage.getItem('token_carx') == null || localStorage.getItem('token_carx') == undefined) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }


}
