import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {CarrosComponent} from './carros/carros.component';
import {routes} from './app.routes';
import {UsuariosComponent} from './usuarios/usuarios.component';
import {NavNlComponent} from './nav-nl/nav-nl.component';
import {NavLComponent} from './nav-l/nav-l.component';
import {NavSLComponent} from './nav-s-l/nav-s-l.component';
import {NavTopSearchBarComponent} from './nav-top-search-bar/nav-top-search-bar.component';
import {NavLogoComponent} from './nav-logo/nav-logo.component';
import {RegistroComponent} from './registro/registro.component';
import {NavTopRegisterComponent} from './nav-top-register/nav-top-register.component';
import {NavTopDefaultComponent} from './nav-top-default/nav-top-default.component';
import {NavTopWithoutSearchBarComponent} from './nav-top-without-search-bar/nav-top-without-search-bar.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UserService} from './services/user.service';
import {ModalModule, OverlayRenderer, DOMOverlayRenderer, Overlay} from 'angular2-modal';
import {Modal, BootstrapModalModule} from 'angular2-modal/plugins/bootstrap';
import {LoginComponent} from './login/login.component';
import {VisualizaComponent} from './carros/visualiza/visualiza.component';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import {SessionGuard} from "./session.guard";
import { UpdateUserComponent } from './admin/update-user/update-user.component';
import { CreateAdsComponent } from './admin/create-ads/create-ads.component';
import { UpdateAdsComponent } from './admin/update-ads/update-ads.component';
import {FilterParser} from "./filter-parser";
import {ImageUploadModule} from "angular2-image-upload";



const MODAL_PROVIDERS = [
  Modal,
  Overlay,
  {provide: OverlayRenderer, useClass: DOMOverlayRenderer}
];

@NgModule({
  declarations: [
    AppComponent,
    CarrosComponent,
    UsuariosComponent,
    NavNlComponent,
    NavLComponent,
    NavSLComponent,
    NavTopSearchBarComponent,
    NavLogoComponent,
    RegistroComponent,
    NavTopRegisterComponent,
    NavTopDefaultComponent,
    NavTopWithoutSearchBarComponent,
    LoginComponent,
    VisualizaComponent,
    AdminComponent,
    DashboardComponent,
    UpdateUserComponent,
    CreateAdsComponent,
    UpdateAdsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    ImageUploadModule.forRoot(),
    ModalModule,
    BootstrapModalModule
  ],
  providers: [UserService, SessionGuard, FilterParser, MODAL_PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule {
}
