import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SessionGuard} from "../../session.guard";
import {AdminComponent} from "../admin.component";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [SessionGuard, AdminComponent]
})

export class DashboardComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private route: ActivatedRoute, private adminComponent: AdminComponent) {
  }

  ngOnInit() {
    this.adminComponent.carrosComponent.doGetBranchs();
    this.adminComponent.carrosComponent.getAds("id,desc", "50", "user_id,=," + this.adminComponent.info_carx.user_id);
  }

  removeModalAds(id) {
    this.adminComponent.modal.confirm().title('Alerta!').body("Tem certeza que deseja deletar este a anuncio ?")
      .okBtn('Delete')
      .cancelBtn('Cancel')
      .open()
      .then(dialog => dialog.result)
      .then(result => {
        this.doRemoveAds(id)
      });
  }

  doRemoveAds(id) {
    this.adminComponent.carrosComponent.carService.doRemoveAds(id)
      .then((response) => {
        this.adminComponent.modalAlert("Deletado com sucesso!");
        this.adminComponent.carrosComponent.getAds("id,desc", "50", "user_id,=," + this.adminComponent.info_carx.user_id);
      })
      .catch((response) => {
        this.adminComponent.modalAlert(response.body);
      });
  }

  fullTextSeach(event) {
    if ((event.target.value !== "" || event.target.value !== null) || event.target.value.length >= 3) {
      this.adminComponent.carrosComponent.ads = null;
      console.log(event.target.value);
      if(event.target.value) {
        let stringSearch =
          "user_id,=,"+this.adminComponent.info_carx.user_id+
          "@fl_active,=,1" +
          "@branch_name,like," + event.target.value +
          "@model_name,like," + event.target.value +
          "@description,like," + event.target.value;
        this.adminComponent.carrosComponent.getAds('id,desc', "10000", stringSearch);
      } else {
        this.adminComponent.carrosComponent.getAds("id,desc", "50", "user_id,=," + this.adminComponent.info_carx.user_id);
      }

    } else {
      this.adminComponent.carrosComponent.getAds("id,desc", "50", "user_id,=," + this.adminComponent.info_carx.user_id);
    }
  }
}
