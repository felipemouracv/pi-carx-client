import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CarService} from "../services/car.service";
import {SessionGuard} from "../session.guard";
import {CarrosComponent} from "../carros/carros.component";
import {UserService} from "../services/user.service";
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {environment} from "../../environments/environment.prod";

declare var $: any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [CarService, SessionGuard, CarrosComponent]
})
export class AdminComponent implements OnInit {
  public info_carx = localStorage.getItem('info_carx') == null ? "" : JSON.parse(localStorage.getItem('info_carx'));
  public env;
  private response;

  constructor(private activatedRoute: ActivatedRoute,
              public route: ActivatedRoute,
              public carrosComponent: CarrosComponent,
              public userService: UserService,
              private router: Router,
              public modal: Modal) {
    this.env = environment;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    //eita
    $('.navbar-brand').attr('href', "/admin");
  }

  doSignout() {
    this.userService.signOut(this.info_carx).then((response) => {
      this.response = response;
      localStorage.removeItem('token_carx');
      localStorage.removeItem('info_carx');
      this.modalAlert(this.response.message);
      this.router.navigate(['/login']);
    }).catch((response) => {
      if (response.status == 401) {
        this.modalAlert("Não autorizado!");
        this.router.navigate(['/login']);
      } else {
        this.modalAlert(this.response.message);
        localStorage.removeItem('token_carx');
        localStorage.removeItem('info_carx');
        this.router.navigate(['/login']);
      }
    })
  }

  doRedirect() {
    localStorage.removeItem('token_carx')
    localStorage.removeItem('info_carx')
    this.modal.alert()
      .title("Alerta!")
      .body("Sua sessão expirou")
      .open();
    this.router.navigate(['/login']);
  }

  modalAlert(message) {
    this.modal.alert()
      .title("Alerta!")
      .body(message)
      .open();
  }

  removeClassMessage(message: string) {
    return message.replace('class java.lang.IllegalArgumentException', '');
  }

  findBranchName(cod) {
    this.carrosComponent.branchs_selected = cod;
    this.carrosComponent.doGetBranchs()
    for (var i = 0; i < this.carrosComponent.branchs.length; i++) {
      console.log(this.carrosComponent.branchs[i].codigo == cod);
      if (this.carrosComponent.branchs[i].codigo === cod) {
        console.log(this.carrosComponent.branchs[i].nome)
        this.carrosComponent.branchs_name = this.carrosComponent.branchs[i].nome;
        return this.carrosComponent.branchs[i].nome;

      }
    }
    return null;
  }

  findModelName(cod) {
    this.carrosComponent.model_selected = cod;
    this.carrosComponent.doGetModel();
    for (var i = 0; i < this.carrosComponent.model.modelos.length; i++) {
      if (this.carrosComponent.model.modelos[i].codigo === cod) {
        this.carrosComponent.model_name = this.carrosComponent.model.modelos[i].nome;
        return this.carrosComponent.model.modelos[i].nome;
      }
    }
    return null;
  }

}
