import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AdminComponent} from "../admin.component";

declare var $: any;

@Component({
  selector: 'app-create-ads',
  templateUrl: './create-ads.component.html',
  styleUrls: ['./create-ads.component.css']
})
export class CreateAdsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private route: ActivatedRoute, private adminComponent: AdminComponent) {
  }

  public cont = 0;

  public url = this.adminComponent.env.url_endpoint_api+'image-ads/admin/';
  public id = null;
  public object = {
    description: '',
    branchKey: '',
    branchName: '',
    modelKey: '',
    modelName: '',
    value:'',
    year: '',
    user_id: this.adminComponent.info_carx.user_id,
    flActive: true
  };


  private response: any;

  ngAfterViewInit() {
    $('#value').mask('000.000.000.000.000,00', {reverse: true});
  }

  ngOnInit() {
    this.adminComponent.carrosComponent.doGetBranchs();
  }

  doNewAds() {
    const value = $('#value').val();
    const value2 = value;
    console.log(value);
    this.object.value = value2.replace(/\./g, '').replace(',','.');
    console.log(this.object.value);
    this.object.branchKey = this.adminComponent.carrosComponent.branchs_selected;
    this.object.modelKey = this.adminComponent.carrosComponent.model_selected;
    this.object.branchName = this.findBranchName(this.adminComponent.carrosComponent.branchs_selected);
    this.object.modelName = this.findModelName(this.adminComponent.carrosComponent.model_selected);
    this.adminComponent.carrosComponent.carService.doSaveAds(this.object)
      .then((response) => {
        this.response = response;
        this.id = this.response.id;
        this.url = this.url+this.id;
        this.object.value = value;
        //$('#value').mask('000.000.000.000.000,00', {reverse: true});
        this.adminComponent.modalAlert('Salvo com sucesso! <br> Você pode fazer upload de imagens clicando no "ENVIAR IMAGENS" ')
      })
      .catch((response) => {
        this.response = response;
        if (this.response.status === 401) {
          this.adminComponent.doRedirect();
        }
        this.adminComponent.modalAlert(this.adminComponent.removeClassMessage(response.body));
      });
  }

  findBranchName(cod) {
    for (var i = 0; i < this.adminComponent.carrosComponent.branchs.length; i++) {
      if (this.adminComponent.carrosComponent.branchs[i].codigo === cod) {
        return this.adminComponent.carrosComponent.branchs[i].nome;
      }
    }
    return null;
  }

  findModelName(cod) {
    for (var i = 0; i < this.adminComponent.carrosComponent.model.modelos.length; i++) {
      if (this.adminComponent.carrosComponent.model.modelos[i].codigo === cod) {
        return this.adminComponent.carrosComponent.model.modelos[i].nome;
      }
    }
    return null;
  }

  imageUploaded(event){
    if(event.serverResponse.status===200) {
      this.adminComponent.modalAlert("Upload com sucesso!");
    }
  }

}
