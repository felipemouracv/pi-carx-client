import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AdminComponent} from "../admin.component";

declare var $: any;

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css'],
  providers: [AdminComponent]
})
export class UpdateUserComponent implements OnInit {

  public password = null;
  public re_password = null;
  public user = {};

  constructor(private activatedRoute: ActivatedRoute, private route: ActivatedRoute, private adminComponent: AdminComponent, private router: Router) {
  }

  ngAfterViewInit() {
    this.adminComponent.info_carx.cpf = this.parseInitMaskCpf(("000000000000" + this.adminComponent.info_carx.cpf).slice(-11));
    $('#cpf').mask('000.000.000-00', {reverse: true});
  }

  ngOnInit() {
  }

  doUpdateUser(event) {
    this.user = {
      id: this.adminComponent.info_carx.user_id,
      name: this.adminComponent.info_carx.name,
      email: this.adminComponent.info_carx.email,
      password: this.password,
      cpf: parseInt(this.removeMaskCpf(this.adminComponent.info_carx.cpf)),
      token: localStorage.getItem('token_carx')
    }
    $.blockUI({message: 'Aguarde...'});
    this.adminComponent.userService.doUpdate(this.user)
      .then((response) => {
        $.unblockUI();
        this.adminComponent.info_carx.token = localStorage.getItem('token_carx');
        this.adminComponent.info_carx.cpf = parseInt(this.removeMaskCpf(this.adminComponent.info_carx.cpf))
        localStorage.setItem('info_carx',JSON.stringify(this.adminComponent.info_carx))
        this.adminComponent.info_carx.cpf = this.parseInitMaskCpf(("000000000000" + this.adminComponent.info_carx.cpf).slice(-11));
        this.adminComponent.modalAlert('Atualizado com sucesso!');
      })
      .catch((response) => {
        $.unblockUI();
        if (response.status === 401) {
          this.adminComponent.doRedirect();
        }
        console.log(response)
        let messageObject = response.body === undefined ? JSON.parse(response._body) : response.body;
        this.adminComponent.modalAlert(this.removeClassMessage(messageObject.message));
      })
  }

  removeClassMessage(message: string) {
    return message.replace('class java.lang.IllegalArgumentException', '');
  }

  parseInitMaskCpf(cpf) {
    return cpf.slice(0, 3) + "." + cpf.slice(3, 6) + "." + cpf.slice(6, 9) + "-" + cpf.slice(9, 11);
  }

  removeMaskCpf(cpf) {
    return cpf.replace('.', '').replace('.', '').replace('-', '');
  }
}
