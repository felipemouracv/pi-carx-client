import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AdminComponent} from "../admin.component";
import {environment} from "../../../environments/environment";

declare var $: any;

@Component({
  selector: 'app-update-ads',
  templateUrl: './update-ads.component.html',
  styleUrls: ['./update-ads.component.css'],
  providers: [AdminComponent]
})
export class UpdateAdsComponent implements OnInit {
  public url = this.adminComponent.env.url_endpoint_api + 'image-ads/admin/';
  public sub;
  public id;
  public env: any = [];
  public ads: any = {
    description: '',
    branchKey: '',
    branchName: '',
    modelKey: '',
    modelName: '',
    value:'',
    year: '',
    user_id: this.adminComponent.info_carx.user_id,
    flActive: true
  };
  public branchName;
  public modelName;
  public response;

  constructor(private activatedRoute: ActivatedRoute, private route: ActivatedRoute, private adminComponent: AdminComponent) {
  }

  ngAfterViewInit() {
    $('#value').mask('000.000.000.000.000,00', {reverse: true});
  }

  ngOnInit() {
    this.adminComponent.carrosComponent.doGetBranchs();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.url = this.url + this.id;
    });
    this.doGetAds();
    console.log(this.adminComponent.carrosComponent.branchs_selected);
    this.env = environment;
  }

  doGetAds() {
    this.adminComponent.carrosComponent.carService.getAds(this.id)
      .then((response) => {
        this.ads = response;
        this.adminComponent.carrosComponent.branchs_selected = this.ads.branchKey;
        this.adminComponent.carrosComponent.model_selected = this.ads.modelKey;
        this.adminComponent.carrosComponent.doGetModel();
        this.ads.value = this.ads.value.toLocaleString("pt-BR", { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }).replace("R$","");
        console.log(this.adminComponent.carrosComponent.branchs_selected);
      }).catch((response) => {
      console.log(response);
    });
  }

  removeModalImage(id) {
    this.adminComponent.modal.confirm().title('Alerta!').body("Tem certeza que deseja deletar esta imagem?")
      .okBtn('Delete')
      .cancelBtn('Cancel')
      .open()
      .then(dialog => dialog.result)
      .then(result => {
        this.doRemoveImage(id)
      });
  }

  doRemoveImage(id) {
    this.adminComponent.carrosComponent.carService.doRemoveImage(id)
      .then((response) => {
        this.adminComponent.modalAlert("Deletado com sucesso!");
        this.doGetAds();
      })
      .catch((response) => {
        this.adminComponent.modalAlert(response.body);
      });
  }

  imageUploaded(event){
    if(event.serverResponse.status===200) {
      this.doGetAds();
      this.adminComponent.modalAlert("Upload com sucesso!");
    }
  }

  doUpdateAds(event){
    event.stopPropagation(); console.log(this.ads);
    const value = $('#value').val();
    const value2 = value;
    console.log(value);
    this.ads.value = value2.replace(/\./g, '').replace(',','.');
    this.ads.branchKey = this.adminComponent.carrosComponent.branchs_selected;
    this.ads.modelKey = this.adminComponent.carrosComponent.model_selected;
    this.ads.branchName = this.findBranchName(this.adminComponent.carrosComponent.branchs_selected);
    this.ads.modelName = this.findModelName(this.adminComponent.carrosComponent.model_selected);
    this.ads.user_id = this.adminComponent.info_carx.user_id;
    console.log(this.ads);
    this.adminComponent.carrosComponent.carService.doEditAds(this.ads)
      .then((response) => {
        this.response = response;
        this.ads = response;
        // this.ads.value = this.ads.value.toLocaleString("pt-BR", { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }).replace("R$","");
        console.log(this.ads);
        this.doGetAds();
        this.adminComponent.modalAlert('Editado com sucesso! <br> Você pode enviar mais imagens clicando no "ENVIAR IMAGENS" ')
      })
      .catch((response) => {
        this.response = response;
        if (this.response.status === 401) {
          this.adminComponent.doRedirect();
        }
        this.adminComponent.modalAlert(this.adminComponent.removeClassMessage(response.body));
      });
  }

  findBranchName(cod) {
    for (var i = 0; i < this.adminComponent.carrosComponent.branchs.length; i++) {
      if (this.adminComponent.carrosComponent.branchs[i].codigo == cod) {
        return this.adminComponent.carrosComponent.branchs[i].nome;
      }
    }
    return null;
  }

  findModelName(cod) {
    for (var i = 0; i < this.adminComponent.carrosComponent.model.modelos.length; i++) {
      if (this.adminComponent.carrosComponent.model.modelos[i].codigo == cod) {
        return this.adminComponent.carrosComponent.model.modelos[i].nome;
      }
    }
    return null;
  }
}
