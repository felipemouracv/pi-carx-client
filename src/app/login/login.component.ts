import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    public modal: Modal,
    private userService: UserService,
    private router: Router
  ) {}

  public loginform: any = {
    email: '',
    password: ''
  };

  public response: any = {};

  ngOnInit() {
  }

  doLogin(event) {
    event.stopPropagation();
    this.userService.doLogin(this.loginform)
      .then(response => {
        this.response = response;
        localStorage.setItem('token_carx',this.response.token);
        localStorage.setItem('info_carx',JSON.stringify(this.response));
        this.router.navigate(['/admin/']);
      })
      .catch((response) => {
        this.modal.alert().title('Alerta!').body('Email ou Senha Invalidos!').open();
      })
  }

}
