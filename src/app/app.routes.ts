/**
 * Created by felipemoura on 29/05/2017.
 */
import {Routes} from '@angular/router';
import {CarrosComponent} from 'app/carros/carros.component';
import {RegistroComponent} from "./registro/registro.component";
import {LoginComponent} from "./login/login.component";
import {VisualizaComponent} from "./carros/visualiza/visualiza.component";
import {AdminComponent} from "./admin/admin.component";
import {DashboardComponent} from "./admin/dashboard/dashboard.component";
import {SessionGuard} from "./session.guard";
import {UpdateUserComponent} from "./admin/update-user/update-user.component";
import {CreateAdsComponent} from "./admin/create-ads/create-ads.component";
import {UpdateAdsComponent} from "./admin/update-ads/update-ads.component";

export const routes: Routes = [
  {path: '', component: CarrosComponent},
  {path: 'view/:id', component: VisualizaComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'admin', component: AdminComponent, children: [
    {
      path: '',
      component: DashboardComponent,
      canActivate: [SessionGuard]
    },{
      path: 'update-user',
      component: UpdateUserComponent,
      canActivate: [SessionGuard]
    },{
      path: 'create-ads',
      component: CreateAdsComponent,
      canActivate: [SessionGuard]
    },{
      path: 'update-ads/:id',
      component: UpdateAdsComponent,
      canActivate: [SessionGuard]
    }
  ]
  }
];
