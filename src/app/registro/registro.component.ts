import {Component, OnInit, ViewContainerRef } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {UserService} from "../services/user.service";
import { Modal } from 'angular2-modal/plugins/bootstrap';

declare var $: any;

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers: [UserService]
})
export class RegistroComponent implements OnInit {

  public name: string = '';
  public password: string = '';
  public email: string = '';
  public re_password: string = '';
  public cpf: any = null;

  public object: any = {};

  constructor(private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private userService: UserService,
              public modal: Modal,
              private router: Router) {
  }

  ngAfterViewInit() {
    $('#cpf').mask('000.000.000-00', {reverse: true});
  }

  ngOnInit() {
  }

  doRegister(event) {
    event.stopPropagation();
    this.object = {
      name: this.name,
      cpf: this.cpf.replace('.', '').replace('.', '').replace('-', ''),
      email: this.email,
      password: this.password,
    };
    $.blockUI({message: 'Aguarde...'});
    this.userService.doRegister(this.object).then((data) => {
      $.unblockUI();
      this.modalAlert('Salvo com sucesso! :) ');
      this.router.navigate(['/login']);
    }).catch((data) => {
      $.unblockUI();
      let messageObject = JSON.parse(data._body);
      this.modalAlert(this.removeClassMessage(messageObject.message));
    })
  }

  removeClassMessage(message: string) {
    return message.replace('class java.lang.IllegalArgumentException', '');
  }
  //TODO: pensar em como abstrair esse modal
  modalAlert( message:string ) {
    this.modal.alert()
      .title('Alerta!')
      .body(message)
      .open();
  }
}
