import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavSLComponent } from './nav-s-l.component';

describe('NavSLComponent', () => {
  let component: NavSLComponent;
  let fixture: ComponentFixture<NavSLComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavSLComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavSLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
