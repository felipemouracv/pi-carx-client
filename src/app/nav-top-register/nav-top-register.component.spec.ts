import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTopRegisterComponent } from './nav-top-register.component';

describe('NavTopRegisterComponent', () => {
  let component: NavTopRegisterComponent;
  let fixture: ComponentFixture<NavTopRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavTopRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavTopRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
