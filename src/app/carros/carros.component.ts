import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CarService} from "../services/car.service";
import {environment} from "../../environments/environment.prod";
import {FilterParser} from "../filter-parser";

declare var $: any;

@Component({
  selector: 'app-carros',
  templateUrl: './carros.component.html',
  styleUrls: ['./carros.component.css'],
  providers: [CarService, FilterParser]
})

export class CarrosComponent implements OnInit {

  public branchs: any = [];

  public branchs_selected: any = '';

  public branchs_name: any = '';

  public model: any = [];

  public model_selected: any = '';
  public model_name: any = '';

  public ads: any = [];

  public env: any = [];

  public year_min = null;

  public year_max = null;

  public range_value_min = null;

  public range_value_max = null;

  public order = "year_desc";

  constructor(private activatedRoute: ActivatedRoute, public carService: CarService, private filterParsed: FilterParser) {
    //this.doGetMarcas();
    this.env = environment;
  }

  ngOnInit() {
    this.doGetBranchs();
    this.getAds("id,desc", "50", "fl_active,=,1");
  }

  ngAfterViewInit() {
    $("#ex2").slider({});
    $('#range_value_min').mask('000000000');
    $('#range_value_max').mask('000000000');
    $('#year_max').mask('0000');
    $('#year_min').mask('0000');
  }

  getAds(order: string, limit: string, cons: string) {
    this.carService.getAdsAll(order, limit, cons)
      .then((response) => {
        this.ads = response;
      }).catch((response) => {
      console.log(response)
    })
  }

  doGetBranchs() {
    this.carService.getBranchs()
      .then((response) => {
        this.branchs = response;
      }).catch((response) => {
      console.log(response);
    })
  }

  doGetModel() {
    if (this.branchs_selected != "") {
      this.carService.getModel(this.branchs_selected)
        .then((response) => {
          this.model = response;
          console.log(this.model);
        }).catch((response) => {
        console.log(response);
      })
    }
  }

  doFilter() {

    this.filterParsed.setOrder(this.order);
    this.filterParsed.setValueMin(this.range_value_min)
    this.filterParsed.setValueMax(this.range_value_max)
    this.filterParsed.setYearMin(this.year_min)
    this.filterParsed.setYearMax(this.year_max)
    this.filterParsed.setBranch(this.branchs_selected)
    this.filterParsed.setModel(this.model_selected)

    this.getAds(this.filterParsed.getUrlParsedOrder(), "100000",
      ("fl_active,=,1:"+this.filterParsed.getUrlParsedCons()).replace(/nul/g,''));
  }

  fullTextSeach(value) {
    if ((value !== "" || value !== null) || value.length >= 3) {
      this.ads = null;
      console.log(value);
      let text = value;
      let stringSearch =
        "fl_active,=,1" +
        "@branch_name,like," + value +
        "@model_name,like," + value +
        "@description,like," + value;
      this.getAds('id,desc', "10000", stringSearch);
    }
  }


}
