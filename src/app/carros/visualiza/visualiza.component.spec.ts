import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizaComponent } from './visualiza.component';

describe('VisualizaComponent', () => {
  let component: VisualizaComponent;
  let fixture: ComponentFixture<VisualizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
