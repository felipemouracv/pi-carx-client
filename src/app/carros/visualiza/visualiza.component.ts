import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CarService} from "../../services/car.service";
import {environment} from "../../../environments/environment.prod";

declare var $: any;

@Component({
  selector: 'app-visualiza',
  templateUrl: './visualiza.component.html',
  styleUrls: ['./visualiza.component.css'],
  providers: [CarService]
})

export class VisualizaComponent implements OnInit {
  public sub;
  public id;
  public env: any = [];
  public ads: any = [];
  public name;
  public email;

  constructor(private activatedRoute: ActivatedRoute, private carService: CarService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.doGetAds();
    this.env = environment;
  }

  ngAfterViewChecked(){
    $(".swipebox").swipebox();
  }

  ngAfterViewInit() {
    $(".swipebox").swipebox();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  doGetAds() {
    this.carService.getAds(this.id)
      .then((response) => {
          this.ads = response;
          this.ads.value = this.ads.value.toLocaleString("pt-BR", { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }).replace("R$","");
          this.email = this.ads.user.email;
          this.name = this.ads.user.name;
      }).catch((response) => {
          console.log(response);
      });
  }

}
