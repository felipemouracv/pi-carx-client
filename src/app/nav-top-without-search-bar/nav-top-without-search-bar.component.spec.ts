import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTopWithoutSearchBarComponent } from './nav-top-without-search-bar.component';

describe('NavTopWithoutSearchBarComponent', () => {
  let component: NavTopWithoutSearchBarComponent;
  let fixture: ComponentFixture<NavTopWithoutSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavTopWithoutSearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavTopWithoutSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
