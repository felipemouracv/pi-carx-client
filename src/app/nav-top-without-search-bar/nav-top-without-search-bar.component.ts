import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-top-without-search-bar',
  templateUrl: './nav-top-without-search-bar.component.html',
  styleUrls: ['./nav-top-without-search-bar.component.css']
})
export class NavTopWithoutSearchBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
