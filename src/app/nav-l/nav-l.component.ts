import {Component, Input, OnInit} from '@angular/core';
import {AdminComponent} from "../admin/admin.component";

@Component({
  selector: 'app-nav-l',
  templateUrl: './nav-l.component.html',
  styleUrls: ['./nav-l.component.css'],
  providers: [AdminComponent]
})
export class NavLComponent implements OnInit {
  @Input() name: string;
  @Input() id: string;
  constructor(public adminComponent: AdminComponent) { }

  ngOnInit() {
  }

  doSignOut() {
    this.adminComponent.doSignout();
  }

}
