import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavLComponent } from './nav-l.component';

describe('NavLComponent', () => {
  let component: NavLComponent;
  let fixture: ComponentFixture<NavLComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavLComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
