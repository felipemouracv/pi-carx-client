export class FilterParser {

  private _order = '';
  private url_parsed_cons = '';
  private _year_max = '';
  private _year_min = '';
  private _model = '';
  private _branch = '';
  private _value_max = '';
  private _value_min = '';

  setOrder(value) {
    this._order = value;
  }

  setYearMax(value) {
    this._year_max = value === null || value === '' ? "null" : "year,<=," + value + ":";
  }

  setYearMin(value) {
    this._year_min = value === null || value === '' ? "null" : "year,>=," + value + ":";
  }

  setModel(value) {
    this._model = value === null || value === '' ? "null" : "model_key,=," + value + ":";
  }

  setBranch(value) {
    this._branch = value === null || value === '' ? "null" : "branch_key,=," + value + ":";
  }

  setValueMax(value) {
    this._value_max = value === null || value === '' ? "null" : "value,<=," + value + ":";
  }

  setValueMin(value) {
    this._value_min = value === null || value == '' ? "null" : "value,>=," + value + ":";
  }

  private orderDict;

  constructor() {

    this.orderDict = {
      value_desc: 'value,desc',
      value_asc: 'value,asc',
      year_asc: 'year,asc',
      year_desc: 'year,desc'
    };
  }


  getUrlParsedCons() {
    return ((this._value_min === null || this._value_min === "null" ? "" : this._value_min + ":") +
    (this._year_max === null || this._year_max === "" ? "null" : this._year_max + ":") +
    (this._value_min === null || this._value_min === "" ? "null" : this._value_min + ":") +
    (this._value_max === null || this._value_max === "" ? "null" : this._value_max + ":") +
    (this._model === null || this._model === "" ? "null" : this._model + ":") +
    (this._branch === null || this._branch === "" ? "null" : this._branch)).replace(/null:/g, '').replace(/::/g,':').replace(/:nul/g,'').replace(":null","").slice(0,-1);
  }

  getUrlParsedOrder() {
    return this.orderDict[this._order];
  }


}
